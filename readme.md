## cdoku

this is a relativley simple and efficent sudoku puzzle brute force algorithm!

check out ./example.sudoku and ./main.c for more documentation:)

### building and running

`just b` to build the binary

check out `just --list` for more options

`cdoku {file path}`

### examples and some bench marks

(forgot to get more than the freq lol)

#### ./example.sudoku
- apline | 1.80 GHz arm64  -  ~1.3s  </br>
- win64  | 1.60 GHz amd64  -  ~0.169s</br>
- gentoo | 5.20 GHz amd64  -  ~0.02s </br>
- gentoo | 2.40 GHz amd64  -  ~0.3s  </br>
#### ./example-hard.sudoku
- gentoo | 5.20 GHz amd64  -  ~54s </br>
- gentoo | 2.40 GHz amd64  -  ~179s</br>
### todo

- [ ] read json formated
- [ ] read sudoku as argument
- [ ] allow for non 9x9 puzzles
- [x] implement validation optimizations
